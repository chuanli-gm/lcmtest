
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exlcm.objsituation_t;
import lcm.lcm.*;

public class ObjSituationGenerator implements LcmMessageGenerator{

	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(ObjSituationGenerator.class);

	public void GenerateDataAndPublish(int i) {

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		objsituation_t objsituation = new objsituation_t();
		if (i % 6 == 0) {
			objsituation.collision_possible = true;
			logger.info("Set collision possible");
		} else if (i % 6 == 1) {
			objsituation.collision_possible = false;
			objsituation.relative_location = (byte) shared_enums.eSAObjLocation.SA_AHEAD_FAR_LEFT.value;
			logger.info("?????");
		} else if (i % 6 == 2) {
			objsituation.collision_possible = false;
			objsituation.relative_location = (byte) shared_enums.eSAObjLocation.SA_INTERSECTING_FROM_LEFT.value;
			logger.info("Set intersection from left");
		} else if (i % 6 == 3) {
			objsituation.relative_location = (byte) shared_enums.eSAObjLocation.SA_LEFT.value;
			logger.info("Set car from left");
		} else if (i % 6 == 4) {
			objsituation.relative_location = (byte) shared_enums.eSAObjLocation.SA_RIGHT.value;
			logger.info("Set car from right");
		} else if (i % 6 == 5) {
			objsituation.relative_location = (byte) shared_enums.eSAObjLocation.SA_ONCOMING_LEFT.value;
			logger.info("Set car oncoming left");
		}
		lcm.publish("OBJSITUATION", objsituation);

	}

	public ObjSituationGenerator() {
		lcm = LcmUtility.getLcm();
	}

}
