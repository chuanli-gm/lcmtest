
import java.util.Random;
import exlcm.vehiclestate_t;
import lcm.lcm.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class VehicleStateGenerator implements LcmMessageGenerator{

	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(VehicleStateGenerator.class);
	private static int vehicle_oem_speed = 0;
	public VehicleStateGenerator() {
		lcm = LcmUtility.getLcm();
	}

	public void GenerateDataAndPublish(int i) {

		vehiclestate_t vehiclestate = new vehiclestate_t();
		Random rand = new Random();
		//vehiclestate.oem_speed_mps = rand.nextInt(80) + 1;
		if (i % 9 == 0) {
			/*vehiclestate.vehicle_autonomous_state = 9;
			vehicle_oem_speed += 1;
			if (vehicle_oem_speed < 0)  vehicle_oem_speed = 0;*/
			
			vehiclestate.oem_speed_mps = vehicle_oem_speed;
			vehiclestate.vehicle_autonomous_state = 0;
			logger.info("Setting vehicle autonomus to OFF_AUTONOMOUS_STATE, speed is artificial.");
		} else if (i % 9 == 1) {
			/*vehicle_oem_speed -= 1;
			if (vehicle_oem_speed < 0)  vehicle_oem_speed = 0;
			vehiclestate.vehicle_autonomous_state = 9;*/
			
			vehiclestate.oem_speed_mps = vehicle_oem_speed;
			//vehiclestate.vehicle_autonomous_state = 1;
			logger.info("Setting vehicle autonomus to READY_TO_ASSIST_AUTONOMOUS_STATE, speed is artificial.");
		} else if (i % 9 == 2) {
			vehiclestate.vehicle_autonomous_state = 2;
			logger.info("Setting vehicle autonomus to ENGAGED_AUTONOMOUS_STATE, speed is artificial.");
		} else if (i % 9 == 3) {
			vehiclestate.vehicle_autonomous_state = 3;
			logger.info("Setting vehicle autonomus to FAIL_AUTONOMOUS_STATE, speed is artificial.");
			
		} else if (i % 9 == 4) {
			vehiclestate.vehicle_autonomous_state = 4;
			logger.info("Setting vehicle autonomus to ABORT_AUTONOMOUS_STATE, speed is artificial.");
			
		} else if (i % 9 == 5) {
			vehiclestate.vehicle_autonomous_state = 5;
			logger.info("Setting vehicle autonomus to DRIVER_OVERRIDE_AUTONOMOUS_STATE, speed is artificial.");
			
		} else if (i % 9 == 6) {
			vehiclestate.vehicle_autonomous_state = 6;
			logger.info("Setting vehicle autonomus to WARNING_AUTONOMOUS_STATE, speed is artificial.");
			
		} else if (i % 9 == 7) {
			vehiclestate.vehicle_autonomous_state = 7;
			logger.info("Setting vehicle autonomus to NOT_READY_TO_ASSIST_AUTONOMOUS_STATE, speed is artificial.");
		}
		else if ( i % 9 == 8){
			vehiclestate.vehicle_autonomous_state = 8;
			logger.info("Setting vehicle autonomus to ACTIVE_AUTONOMUS_STATE, speed is artificial.");
		}
		

		lcm.publish("VEHICLESTATE", vehiclestate);

	}

}
