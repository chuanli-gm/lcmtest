import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.slf4j.LoggerFactory;


public class TestDriver implements NativeKeyListener {
	enum STATE {LANESET, VEHICLE_STATE, BEHAVIORPLAN, FUSIONOBSTACLE, LANESEGMENT, OBJSITUATION, PATHPLAN, SITUATION,POSITIONPOSE, UNSET};
	private org.slf4j.Logger logger = LoggerFactory.getLogger(TestDriver.class);
	private STATE commandState = STATE.UNSET;

	public TestDriver() throws IOException {

		// Get the logger for "org.jnativehook" and set the level to off.
		Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
		logger.setLevel(Level.OFF);

		// Change the level for all handlers attached to the default logger.
		Handler[] handlers = Logger.getLogger("").getHandlers();
		for (int i = 0; i < handlers.length; i++) {
		    handlers[i].setLevel(Level.OFF);
		}
	}

	/**
	 * Overridden method to capture the pressed keys.
	 */
	@Override
	public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
		//logger.info("Key pressed: " + NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode()));
		//logger.info("Key pressed: " + NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode()));
		String keyPressed = NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode());
		boolean keyHandled = false;
		if ( keyPressed.toLowerCase().equals("l")){
			keyHandled = true;
			commandState = STATE.LANESET;
			logger.info("LANSET enabled. Available commands: "
					+ "\n\t\t 0: do not use "
					+ "\n\t\t 1: best guess "
					+ "\n\t\t 2: fair "
					+ "\n\t\t 3: good "
					+ "\n\t\t 4: excellent");
			return;
			
		}
		else if (keyPressed.toLowerCase().equals("u")){
			keyHandled = true;
			commandState = STATE.UNSET;
			logger.info("Command state reset");
			return;
		}
		else if (keyPressed.toLowerCase().equals("v")){
			keyHandled = true;
			commandState = STATE.VEHICLE_STATE;
			logger.info("VEHICLE_STATE enabled. Available commands: "
					+ "\n\t\t 0: OFF "
					+ "\n\t\t 1: READY TO ASSIST "
					+ "\n\t\t 2: ENGAGED "
					+ "\n\t\t 3: FAIL "
					+ "\n\t\t 4: ABORT"
					+ "\n\t\t 5: DRIVER OVERRIDE"
					+ "\n\t\t 6: WARNING"
					+ "\n\t\t 7: NOT READY TO ASSIT"
					+ "\n\t\t 8: ACTIVE");
			return;
		}
		else if ( keyPressed.toLowerCase().equals("b")){
			keyHandled = true;
			commandState = STATE.BEHAVIORPLAN;
			logger.info("BEHAVIORPLAN enabled. Available commands: "
					+ "\n\t\t 0: set target speed to 30 MPH"
					+ "\n\t\t 1: set target speed to 15 MPH"
					+ "\n\t\t 2: set target speed to 0 MPH"
					+ "\n\t\t 3: set keep lane maneuver"
					+ "\n\t\t 4: set turn left maneuver"
					+ "\n\t\t 5: set turn right maneuver"
					+ "\n\t\t 6: set change lane to left maneuver"
					+ "\n\t\t 7: set change lane to right maneuver");
		}
		else if ( keyPressed.toLowerCase().equals("f")){
			keyHandled = true;
			commandState = STATE.FUSIONOBSTACLE;
			logger.info("FUSIONOBSTACLE enabled. Available commands: "
					+ "\n\t\t 0: set obstacle type as pedestrian, 6 meters ahead"
					+ "\n\t\t 1: set obstacle type as vehicle, 10 meters ahead");
		}
		else if ( keyPressed.toLowerCase().equals("g")){
			keyHandled = true;
			commandState = STATE.LANESEGMENT;
			logger.info("LANESEGMENT enabled. Available commands: "
					+ "\n\t\t 0: set speed limit to 30 MPH"
					+ "\n\t\t 1: set stop sign restriction"
					+ "\n\t\t 2: set yield sign restriction"
					+ "\n\t\t 3: set pedestrian cross sign restriction");
		}
		else if ( keyPressed.toLowerCase().equals("o")){
			keyHandled = true;
			commandState = STATE.OBJSITUATION;
			logger.info("OBJSITUATION enabled. Available commands: "
					+ "\n\t\t 0: set collision possible"
					+ "\n\t\t 1: ?????"
					+ "\n\t\t 2: set intersection from left"
					+ "\n\t\t 3: set car from left"
					+ "\n\t\t 4: set car from right"
					+ "\n\t\t 5: set car oncoming left");
		}
		else if ( keyPressed.toLowerCase().equals("p")){
			keyHandled = true;
			commandState = STATE.PATHPLAN;
			logger.info("PATHPLAN enabled. Available commands: "
					+ "\n\t\t 0: set straight direction"
					+ "\n\t\t 1: set bear left"
					+ "\n\t\t 2: set turn left"
					+ "\n\t\t 3: set bear right"
					+ "\n\t\t 4: set turn right");
		}
		else if ( keyPressed.toLowerCase().equals("s")){
			keyHandled = true;
			commandState = STATE.SITUATION;
			logger.info("SITUATION enabled. Available commands: "
					+ "\n\t\t 0: set traffic lights RED"
					+ "\n\t\t 1: set traffic lights YELLOW"
					+ "\n\t\t 2: set traffic lights GREEN");
		}
		else if ( keyPressed.toLowerCase().equals("a")){
			keyHandled = true;
			commandState = STATE.POSITIONPOSE;
			logger.info("POSITIONPOSE enabled, just send");
		}
		//==========================HANDLE in-command variables... 
		int cmd = Integer.MIN_VALUE;
		try {
			cmd = Integer.valueOf(keyPressed);
			if ( commandState == STATE.LANESET){
				LaneSetGenerator g = new LaneSetGenerator ();
				if ( cmd >= 0 && cmd <= 4){

					keyHandled = true;
					g.GenerateDataAndPublish(cmd);
				}
			}
			else if ( commandState == STATE.VEHICLE_STATE){
				VehicleStateGenerator g = new VehicleStateGenerator ();
				if ( cmd >= 0 && cmd <= 8 ){
					keyHandled = true;
					g.GenerateDataAndPublish (cmd);
				}
			}
			else if ( commandState == STATE.BEHAVIORPLAN){
				BehaviorPlanGenerator g = new BehaviorPlanGenerator ();
				if (cmd >= 0 && cmd <= 7)
				{
					keyHandled = true;
					g.GenerateDataAndPublish (cmd);
				}
			}
			else if ( commandState == STATE.FUSIONOBSTACLE){
				FusionObjectListGenerator g = new FusionObjectListGenerator ();
				if (cmd >= 0 && cmd <= 1)
				{
					keyHandled = true;
					g.GenerateDataAndPublish (cmd);
				}
			}
			else if (commandState == STATE.LANESEGMENT){
				LaneSegmentGenerator g = new LaneSegmentGenerator ();
				if (cmd >= 0 && cmd <= 3)
				{
					keyHandled = true;
					g.GenerateDataAndPublish (cmd);
				}
			}
			else if ( commandState == STATE.OBJSITUATION){
				ObjSituationGenerator g = new ObjSituationGenerator ();
				if (cmd >= 0 && cmd <= 5)
				{
					keyHandled = true;
					g.GenerateDataAndPublish (cmd);
				}
			}
			else if ( commandState == STATE.PATHPLAN){
				PathPlanGenerator g = new PathPlanGenerator ();
				if (cmd >= 0 && cmd <= 4)
				{
					keyHandled = true;
					g.GenerateDataAndPublish (cmd);
				}
			}
			else if ( commandState == STATE.SITUATION){
				SituationGenerator g = new SituationGenerator ();
				if (cmd >= 0 && cmd <= 4)
				{
					keyHandled = true;
					g.GenerateDataAndPublish (cmd);
				}
			}
			else if (commandState == STATE.POSITIONPOSE){
				logger.info("publising ..... ");
				PositionPoseGenerator p = new PositionPoseGenerator ();
				p.GenerateDataAndPublish(cmd);
			}
		}
		catch (NumberFormatException e){
			
		}
		
		if ( keyHandled == false){
			logger.warn("Command not recognized");
		}
	}

	/**
	 * Overridden method to capture released keys.
	 */
	@Override
	public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
	}

	/**
	 * Overridden method to capture the typed keys.0
	 */
	@Override
	public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
	}

	/**
	 * The main method.
	 * @param arguments - Command-line arguments
	 * @throws IOException
	 * @throws NativeHookException
	 */
	public static void main(String arguments[]) throws IOException, NativeHookException {
		GlobalScreen.registerNativeHook();
		GlobalScreen.addNativeKeyListener(new TestDriver());
	}
}