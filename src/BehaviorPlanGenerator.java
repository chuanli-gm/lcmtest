
//import shared_enums;
import java.io.*;
import lcm.lcm.*;
import exlcm.behaviorplan_t;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class BehaviorPlanGenerator implements LcmMessageGenerator{
	private LCM lcm;
	static private int target_speed = 0;
	private Logger logger = LoggerFactory.getLogger(BehaviorPlanGenerator.class);

	public BehaviorPlanGenerator() {
		lcm = LcmUtility.getLcm();
	}

	public void GenerateDataAndPublish(int i) {

		behaviorplan_t behaviorplan = new behaviorplan_t();
		if (i % 8 == 0) {
			logger.info("Setting target speed to 30 MPH");
			behaviorplan.speed_target_mps = 30;
			target_speed = 30;
		} else if (i % 8 == 1) {
			behaviorplan.speed_target_mps = 15;
			target_speed = 15;
			logger.info("Setting target speed to 15 MPH");
		} else if (i % 8 == 2) {
			behaviorplan.speed_target_mps = 0;
			target_speed = 0;
			logger.info("Setting target speed to 0 MPH");
		} else if (i % 8 == 3) {
			behaviorplan.active_maneuver = (short) shared_enums.eActiveManeuver.LANE_KEEPING_MANEUVER.value;
			behaviorplan.speed_target_mps = target_speed;
			logger.info("Setting maneuver to LANE_KEEPING type");
		} else if (i % 8 == 4) {
			behaviorplan.behavior_type = (short) shared_enums.eBehaviorType.TURN_LEFT_BEHAVIOR.value;
			behaviorplan.speed_target_mps = target_speed;
			logger.info("Setting maneuver to TURN_LEFT type");
			// behaviorplan.speed_target_mps = 30;
		} else if (i % 8 == 5) {
			behaviorplan.behavior_type = (short) shared_enums.eBehaviorType.TURN_RIGHT_BEHAVIOR.value;
			behaviorplan.speed_target_mps = target_speed;
			logger.info("Setting maneuver to TURN_RIGHT type");
			// behaviorplan.speed_target_mps = 30;
		} else if (i % 8 == 6) {
			// behaviorplan.speed_target_mps = 30;
			behaviorplan.left_lane_intention = (short) shared_enums.eLaneIntention.LANE_CHANGE_IS_MANDATORY.value;
			behaviorplan.speed_target_mps = target_speed;
			logger.info("Setting maneuver to LEFT_LANE_CHANGING_IS_MANDATORY type");
		} else if (i % 8 == 7) {
			// behaviorplan.speed_target_mps = 30;
			behaviorplan.right_lane_intention = (short) shared_enums.eLaneIntention.LANE_CHANGE_IS_MANDATORY.value;
			behaviorplan.speed_target_mps = target_speed;
			logger.info("Setting maneuver to RIGHT_LANE_CHANGE_IS_MANDATORY type");
		}
		lcm.publish("BEHAVIORPLAN", behaviorplan);
	}

}
