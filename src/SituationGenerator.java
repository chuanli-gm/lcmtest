
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exlcm.situation_t;

import lcm.lcm.*;

public class SituationGenerator implements LcmMessageGenerator {
	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(SituationGenerator.class);

	public void GenerateDataAndPublish(int i) {

		situation_t situation = new situation_t();

		if (i % 3 == 0) {
			situation.hv_traffic_signal_phase = (short) shared_enums.eTrafficSignalState.SOLID_RED_SIGNAL_STATE.value;
			logger.info("Set traffic lights RED");
		} else if (i % 3 == 1) {
			situation.hv_traffic_signal_phase = (short) shared_enums.eTrafficSignalState.YELLOW_SIGNAL_STATE.value;
			logger.info("Set traffic lights YELLOW");
		} else if (i % 3 == 2) {
			situation.hv_traffic_signal_phase = (short) shared_enums.eTrafficSignalState.GREEN_SIGNAL_STATE.value;
			logger.info("Set traffic lights GREEN");
		}

		lcm.publish("SITUATION", situation);
	}

	public SituationGenerator() {
		lcm = LcmUtility.getLcm();
	}

}
