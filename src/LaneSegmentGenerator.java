
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exlcm.connection_points_t;
import exlcm.connection_t;
import exlcm.edgeset_t;
import exlcm.laneflowrestriction_t;
import exlcm.lanesegment_t;
import exlcm.lanewidth_t;
import exlcm.point_enu_t;
import exlcm.waypoint_t;

import lcm.lcm.*;

public class LaneSegmentGenerator implements LcmMessageGenerator{

	/**
	 * @param args
	 */
	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(LaneSegmentGenerator.class);

	public LaneSegmentGenerator() {
		lcm = LcmUtility.getLcm();
	}

	public void GenerateDataAndPublish(int i) {

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lanesegment_t lanesegment = new lanesegment_t();
		lanesegment.lane_width = new lanewidth_t[10];
		lanesegment.lane_flow_restriction_count = 10;
		for (int j = 0; j < 10; ++j) {
			lanesegment.lane_width[j] = new lanewidth_t();
		}
		lanesegment.lane_flow_restriction = new laneflowrestriction_t[10];
		for (int j = 0; j < 10; ++j) {
			lanesegment.lane_flow_restriction[j] = new laneflowrestriction_t();
		}
		lanesegment.nominal_path_waypoint = new waypoint_t[100];
		for (int j = 0; j < 100; ++j) {
			lanesegment.nominal_path_waypoint[j] = new waypoint_t();
		}
		lanesegment.left_edge_set = new edgeset_t[10];
		for (int j = 0; j < 10; ++j) {
			lanesegment.left_edge_set[j] = new edgeset_t();
			lanesegment.left_edge_set[j].edge_points = new point_enu_t[10];
			for (int k = 0; k < 10; ++k) {
				lanesegment.left_edge_set[j].edge_points[k] = new point_enu_t();
			}
		}

		lanesegment.right_edge_set = lanesegment.left_edge_set;
		lanesegment.connection = new connection_t[8];
		for (int j = 0; j < 8; ++j) {
			lanesegment.connection[j] = new connection_t();
			for (int k = 0; k < 16; ++k) {
				lanesegment.connection[j].point[k] = new connection_points_t();
			}
		}

		if (i % 4 == 0) {
			lanesegment.posted_speed_lim_mps = 30;
			logger.info("Set speed limit to 30 MPH");
		} else if (i % 4 == 1) {
			for (laneflowrestriction_t restriction : lanesegment.lane_flow_restriction) {
				restriction.lane_flow_restriction_type = (short) shared_enums.eLaneFlowRestrictionType.STOP_SIGN_RESTRICTION.value;
			}
			logger.info("Set stop sign restriction");
		} else if (i % 4 == 2) {
			for (laneflowrestriction_t restriction : lanesegment.lane_flow_restriction) {
				restriction.lane_flow_restriction_type = (short) shared_enums.eLaneFlowRestrictionType.YIELD_SIGN_RESTRICTION.value;
			}
			logger.info("Set yield sign restriction");
		} else if (i % 4 == 3) {
			for (laneflowrestriction_t restriction : lanesegment.lane_flow_restriction) {
				restriction.lane_flow_restriction_type = (short) shared_enums.eLaneFlowRestrictionType.PEDESTRIAN_CROSSING_RESTRICTION.value;
			}
			logger.info("Set pedestrian cross sign restriction");
		}
		lcm.publish("LANESEGMENT", lanesegment);

	}

}
