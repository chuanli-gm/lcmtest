
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exlcm.pathplan_t;
import exlcm.pathstep_t;

import lcm.lcm.*;

public class PathPlanGenerator implements LcmMessageGenerator{
	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(PathPlanGenerator.class);

	public PathPlanGenerator() {
		lcm = LcmUtility.getLcm();

	}

	public void GenerateDataAndPublish(int i) {
		// TODO Auto-generated method stub

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pathplan_t pathplan = new pathplan_t();
		pathplan.path_steps = new pathstep_t[200];
		pathplan.included_step_count = 200;

		if (i % 5 == 0) {
			for (int j = 0; j < 200; ++j) {
				pathplan.path_steps[j] = new pathstep_t();
				pathplan.path_steps[j].direction = (short) shared_enums.eDirection.STRAIGHT_DIRECTION.value;
			}
			logger.info("Set straight direction");
		} else if (i % 5 == 1) {
			for (int j = 0; j < 200; ++j) {
				pathplan.path_steps[j] = new pathstep_t();
				pathplan.path_steps[j].direction = (short) shared_enums.eDirection.BEAR_LEFT.value;
			}
			logger.info("Set bear left");
		} else if (i % 5 == 2) {
			for (int j = 0; j < 200; ++j) {
				pathplan.path_steps[j] = new pathstep_t();
				pathplan.path_steps[j].direction = (short) shared_enums.eDirection.TURN_LEFT.value;
			}
			logger.info("Set turn left");
		} else if (i % 5 == 3) {
			for (int j = 0; j < 200; ++j) {
				pathplan.path_steps[j] = new pathstep_t();
				pathplan.path_steps[j].direction = (short) shared_enums.eDirection.BEAR_RIGHT.value;
			}
			logger.info("Set bear right");
		} else if (i % 5 == 4) {
			for (int j = 0; j < 200; ++j) {
				pathplan.path_steps[j] = new pathstep_t();
				pathplan.path_steps[j].direction = (short) shared_enums.eDirection.TURN_RIGHT.value;
			}
			logger.info("Set turn right");
		}
		lcm.publish("PATHPLAN", pathplan);

	}

}
