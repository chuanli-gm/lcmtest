

import exlcm.fusionobjectlist_t;
import exlcm.obstacle_t;
import exlcm.stixel_t;
import lcm.lcm.*;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class FusionObjectListGenerator implements LcmMessageGenerator{

	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(FusionObjectListGenerator.class);

	public FusionObjectListGenerator ()
	{
		lcm = LcmUtility.getLcm();
	}

	public void GenerateDataAndPublish(int i) {
		
			obstacle_t obstacle = new obstacle_t ();
			obstacle.convex_hull_npts = 1;
			obstacle.stixel_npts = 1;
			obstacle.convex_hull_point = new stixel_t [1];
			obstacle.convex_hull_point[0] = new stixel_t ();
			obstacle.stixel_point = new stixel_t [1];
			obstacle.stixel_point[0] = new stixel_t ();
			
			if ( i % 2 == 0)
			{
				
				obstacle.rel_lane = (byte)shared_enums.eFusionRelativeLane.FUSION_REL_LANE_HOST.value;
				obstacle.objtype = (byte)shared_enums.eFusionObstacleType.PEDESTRIAN_OBSTACLE_TYPE.value;
				obstacle.closest_y_m = 6;
				
				logger.info("Set obstacle type as pedestrian, distance 40 meters away");
			}
			else if ( i % 2 == 1)
			{
			
				obstacle.rel_lane = (byte)shared_enums.eFusionRelativeLane.FUSION_REL_LANE_HOST.value;
				obstacle.objtype = (byte)shared_enums.eFusionObstacleType.VEHICLE_OBSTACLE_TYPE.value;
				obstacle.closest_y_m = 10;
				
				logger.info("Set obstacle type as vehicle, distance 20 meters away");
			}
			lcm.publish("FUSIONOBSTACLE", obstacle);
			
			
			/*
		
			fusionobjectlist_t fusionobjectlist = new fusionobjectlist_t ();
			fusionobjectlist.entity_type = 20;
			fusionobjectlist.version_n=1;
			fusionobjectlist.valid_f=1;
			fusionobjectlist.ref_n=1;
			fusionobjectlist.timestamp_sec = 100;
			fusionobjectlist.obstacle = new obstacle_t[64];
			fusionobjectlist.num_obstacles = 64;
			for (int j = 0; j < 64; ++ j)
			{
				fusionobjectlist.obstacle[j] = new obstacle_t ();
				fusionobjectlist.obstacle[j].convex_hull_npts = 1;
				fusionobjectlist.obstacle[j].stixel_npts = 1;
				fusionobjectlist.obstacle[j].convex_hull_point = new stixel_t [1];
				fusionobjectlist.obstacle[j].convex_hull_point[0] = new stixel_t ();
				fusionobjectlist.obstacle[j].stixel_point = new stixel_t [1];
				fusionobjectlist.obstacle[j].stixel_point[0] = new stixel_t ();
			}
			
			if ( i % 2 == 0)
			{
				for (int j = 0; j < 64; ++ j)
				{
					fusionobjectlist.obstacle[j].rel_lane = (byte)shared_enums.eFusionRelativeLane.FUSION_REL_LANE_HOST.value;
					fusionobjectlist.obstacle[j].objtype = (byte)shared_enums.eFusionObstacleType.PEDESTRIAN_OBSTACLE_TYPE.value;
					fusionobjectlist.obstacle[j].closest_y_m = 40;
				}
				logger.info("Set obstacle type as pedestrian, distance 40 meters away");
			}
			else if ( i % 2 == 1)
			{
				for (int j = 0; j < 64; ++ j)
				{
					fusionobjectlist.obstacle[j].rel_lane = (byte)shared_enums.eFusionRelativeLane.FUSION_REL_LANE_HOST.value;
					fusionobjectlist.obstacle[j].objtype = (byte)shared_enums.eFusionObstacleType.VEHICLE_OBSTACLE_TYPE.value;
					fusionobjectlist.obstacle[j].closest_y_m = 20;
				}
				logger.info("Set obstacle type as vehicle, distance 20 meters away");
			}
			lcm.publish("FUSIONOBSTACLE", fusionobjectlist);
			*/
		}
	

}
