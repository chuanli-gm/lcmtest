import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exlcm.positionpose_t;

import lcm.lcm.*;


public class PositionPoseGenerator implements LcmMessageGenerator {
	
	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(PathPlanGenerator.class);

	public PositionPoseGenerator() {
		lcm = LcmUtility.getLcm();

	}
	
	@Override
	public void GenerateDataAndPublish(int cmd) {
		
		positionpose_t positionpose = new positionpose_t ();
		if (cmd == 0){
			positionpose.latitude_deg = 43;
			positionpose.longitude_deg= -83;
		}
		else if ( cmd == 1){
			positionpose.latitude_deg = 42.511897;
			positionpose.longitude_deg= -83.044359;
		}
		lcm.publish("POSITIONPOSE", positionpose);
		logger.info("positionpose published");
	}

}
