import java.io.IOException;

import lcm.lcm.*;
public class LcmUtility {
	static private LCM lcm = null;
	private static void initializeLcm (){
		try 
		{
			SysParameters.init("settings.xml");
			SysParameters sysParameters = SysParameters.getInstance();
			String lcmURL = sysParameters.getParameter("/lcmtest/LcmUtility/LcmBroadcastURL");
			System.out.println (System.getProperty("user.dir"));
			System.out.println(lcmURL);
			lcm = new LCM (lcmURL);
		}
		catch (IOException e)
		{
			System.out.println("Exception setting up LCM: " + e);
		}
	}
	public static LCM getLcm (){
		if (lcm == null){
			initializeLcm ();
		}
		if ( lcm == null){
			System.out.println("Cannot create a LCM instance");
		}
		return lcm;
	}
}
