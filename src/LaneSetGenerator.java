
import exlcm.laneset_t;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import exlcm.laneboundary_t;
import exlcm.point_xyz_t;
import exlcm.pointlist_t;

import lcm.lcm.*;

public class LaneSetGenerator implements LcmMessageGenerator{

	private static byte L_MARK_WHITE = 0x00;
	private static byte L_MARK_YELLOW = 0x01;
	private static byte L_MARK_BLUE = 0x02;
	private static byte L_MARK_INVALID = 0x03;
	
	private static byte L_MARK_UNDECIDED=0x00;
	private static byte L_MARK_SOLID=0x08;
	private static byte L_MARK_EDGE=0x10;
	private static byte L_MARK_DASHED=0x18;
	private static byte L_MARK_DOUBLE_LINE=0x20;
	
	private LCM lcm;
	private Logger logger = LoggerFactory.getLogger(LaneSetGenerator.class);

	public LaneSetGenerator() {
		lcm = LcmUtility.getLcm();
	}

	public void GenerateDataAndPublish(int i) {

		laneset_t laneset = new laneset_t();
		laneset.boundary = new laneboundary_t[4];
		laneset.boundary[0] = new laneboundary_t();
		laneset.boundary[1] = new laneboundary_t();
		laneset.boundary[2] = new laneboundary_t();
		laneset.boundary[3] = new laneboundary_t();

		pointlist_t pointlist = new pointlist_t();
		pointlist.num_points = 100;
		pointlist.point = new point_xyz_t[100];
		for (int j = 0; j < 100; ++j) {
			pointlist.point[j] = new point_xyz_t();
			pointlist.point[j].x_m = 1;
			pointlist.point[j].y_m = 1;
			pointlist.point[j].z_m = 1;
		}

		laneset.boundary[0].polyline = pointlist;
		laneset.boundary[1].polyline = pointlist;
		laneset.boundary[2].polyline = pointlist;
		laneset.boundary[3].polyline = pointlist;
		
		laneset.boundary[0].type = (byte) (L_MARK_YELLOW | L_MARK_DOUBLE_LINE);
		laneset.boundary[1].type = (byte) (L_MARK_WHITE | L_MARK_SOLID);
		
		if (i % 5 == 0) {
			laneset.boundary[0].quality = 0;
			laneset.boundary[1].quality = 0;
			logger.info("Setting laneset quality do not use");
		} else if (i % 5 == 1) {
			laneset.boundary[0].quality = 1;
			laneset.boundary[1].quality = 1;
			logger.info("Setting laneset quality best guess");
		} else if (i % 5 == 2) {
			laneset.boundary[0].quality = 2;
			laneset.boundary[1].quality = 2;
			logger.info("Setting laneset quality fair");
		} else if (i % 5 == 3) {
			laneset.boundary[0].quality = 3;
			laneset.boundary[1].quality = 3;
			logger.info("Setting laneset quality good");
		} else if (i % 5 == 4) {
			laneset.boundary[0].quality = 4;
			laneset.boundary[1].quality = 4;
			logger.info("Setting laneset quality excellent");
		}
		
		lcm.publish("LANESET", laneset);

	}

}
